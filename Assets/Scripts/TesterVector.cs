﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TesterVector : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Vector v = new Vector(2, 4, 5);
        Vector w = new Vector(-2, 3, 4);
        Vector z = new Vector(3, 3, 7);

        Debug.Log("a) " + (v + w));
        Debug.Log("b) " + (w - v));
        Debug.Log("c) " + (z*3));
        Debug.Log("d) " + (v/3));
        Debug.Log("e) " + (v * z));
        Debug.Log("f) " + (w ^ z));
        Debug.Log("g) " + (z ^ w));
        Debug.Log("h) " + (z * (w ^ v)));

    }

}
