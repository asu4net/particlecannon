﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vector : Object
{
    public float x;
    public float y;
    public float z;

    #region constructors
    public Vector()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    public Vector(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector(float AngleX, float AngleY, float AngleZ, float length)
    {
        float checkDirectionCosines = Mathf.Pow(Mathf.Cos(AngleX), 2) + Mathf.Pow(Mathf.Cos(AngleY), 2) + Mathf.Pow(Mathf.Cos(AngleZ), 2);
        
        if(checkDirectionCosines > 0.999f && checkDirectionCosines < 1.001f)
        {
            x = length * Mathf.Cos(AngleX);
            y = length * Mathf.Cos(AngleY);
            z = length * Mathf.Cos(AngleZ);
        }
        else
        {
            throw new System.Exception("Wrong Direction Angles");
        }       
        
    }
    #endregion

    #region methods

    public float Magnitude()
    { 
        return Mathf.Sqrt(x * x + y * y + z * z);
    }
    
    public void Normalize()
    {
        float magnitude = Magnitude();
        if (magnitude == 0) return;
        x /= magnitude;
        y /= magnitude;
        z /= magnitude;
    }

    public void Reverse()
    {
        x = -x;
        y = -y;
        z = -z;
    }

    public static Vector operator +(Vector a, Vector b)
    {
        return new Vector(a.x + b.x, + a.y + b.y, a.z + b.z);
    }

    public static Vector operator -(Vector a, Vector b)
    {
        return new Vector(a.x - b.x, - a.y - b.y, a.z - b.z);
    }
   
    public static Vector operator *(Vector a, float b)
    {
        return new Vector(a.x * b, -a.y * b, a.z * b);
    }

    public static Vector operator /(Vector a, float b)
    {
        return new Vector(a.x / b, -a.y / b, a.z / b);
    }

    public static float operator *(Vector a, Vector b)
    {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    public static Vector operator ^(Vector a, Vector b)
    {
        return new Vector(a.y * b.z - a.z * b.y, a.x * b.z + a.z * b.x, a.x * b.y - a.y * b.x);
    }

    public override string ToString()
    {
        return "{" + x + ", " + y + ", " + z + "}";
    }

    public Vector3 toVector3()
    {
        return new Vector3(x, y, z);
    }

    #endregion
}
