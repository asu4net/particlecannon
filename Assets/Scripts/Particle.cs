﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle : MonoBehaviour
{
    public float initialVelocity;
    public float initialAngle;
    public float maxLifeTime;
    public float lifeTime;
    public float gravity;  

    public Particle(float initialVelocity, float initialAngle, float maxLifeTime, float lifeTime, float gravity)
    {

    }
}
