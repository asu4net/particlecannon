﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    public int maxParticles = 50;
    public GameObject prefab;
    private List<GameObject> particles = new List<GameObject>();

    public float initialVelocity;
    public float initialAngle;
    public float lifeTime;
    public float gravity;

    private bool @switch;


    void FixedUpdate()
    {
        if(@switch)
        {
            for (int i = 0; i < particles.Count; i++)
            {
                Vector vector = DoSimulation(particles[i].GetComponent<Particle>());
                if (particles[i].GetComponent<Particle>().lifeTime >= particles[i].GetComponent<Particle>().maxLifeTime)
                {
                    GameObject gameObjectF = particles[i];
                    particles.RemoveAt(i);
                    Destroy(gameObjectF);
                } else
                {
                    particles[i].transform.position = vector.toVector3();
                }

            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            CreateParticle(initialVelocity, initialAngle, lifeTime, gravity);
            @switch = true;
        }        
    }

    void CreateParticle(float initialVelocity, float initialAngle, float lifeTime, float gravity)
    {
        this.initialVelocity = initialVelocity; 

        for(int i = 0; i < maxParticles; i++)
        {
            GameObject particle = Instantiate(prefab);
            Particle particleScript = particle.GetComponent<Particle>();

            particle.transform.position = Vector3.zero;
            particleScript.initialVelocity = UnityEngine.Random.Range(initialVelocity/2, initialVelocity);

            if (initialAngle > 999)
            {
                particleScript.initialAngle = initialAngle;
            } else
            {
                particleScript.initialAngle = UnityEngine.Random.Range(0, 360);
            }

            particleScript.maxLifeTime = UnityEngine.Random.Range(initialVelocity / 2, initialVelocity);
            particleScript.lifeTime = 0;
            particleScript.gravity = gravity;
            particles.Add(particle);
        }    
    }

    Vector DoSimulation(Particle particle)
    {
        Vector vector = new Vector();
        particle.lifeTime += Time.deltaTime;

        vector.x = particle.initialVelocity * Mathf.Cos(particle.initialAngle * Mathf.PI / 180f) * particle.lifeTime;
        vector.z = particle.initialVelocity * Mathf.Cos(particle.initialAngle * Mathf.PI / 180f) * particle.lifeTime + (particle.gravity * particle.lifeTime * particle.lifeTime) /2f;
        vector.y = 0;

        return vector;
    }

}
